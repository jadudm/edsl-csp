Exploring a CSP-ish EDSL.

Code like

```racket

(proc (consumer [ch? chan:int])
      (while true
             (store val (get ch?))
             ))

(proc (producer [ch! chan:int])
      (variables ([x int])
                 (seq
                  (store x 0)
                  (while (< x 10)
                         (seq
                          (put ch! x)
                          (store x (+ x 1))
                          )))))

(proc (main)
      (channels ([ch chan:int])
                (seq
                 (consumer ch?)
                 (producer ch!))))
```

should be possible. (That would deadlock.)