#lang racket

(require syntax/parse
         (for-syntax syntax/parse))

(require (prefix-in L0: "../lang/L0.rkt"))

#;(provide
 (contract-out
  [to-list-of-stx (input-port? . -> . list-of-syntax?)]
  [stx-to-structs (list-of-syntax? . -> . L0:identity)]
  ))

(provide to-list-of-stx stx-to-structs L0:identity)

;; CONTRACT HELPERS
(define (list-of-syntax? ls)
  (andmap syntax? ls))

(define (read-all-stx inp)
  ;; (define inp (open-input-file f))
  (define stxs '())
  (let loop ([stx (read-syntax
                   inp inp)])
    (unless (eof-object? stx)
      (set! stxs (cons stx stxs))
      (loop (read-syntax inp inp))))
  (reverse stxs))

;; CONTRACT
;; pass-to-struct :: input-port -> L0
;; PURPOSE
;; Reads a file in as a list of structs.
(define (to-list-of-stx inp)
  (read-all-stx inp))

(define (stx->binding stx)
  (syntax-case stx ()
    [(id type)
     (L0:formal (L0:meta #'id)
                (syntax-e #'id)
                (syntax-e #'type))]))

;; Numbers, strings, booleans, keywords, and the empty list match as literals.
(define (stx->actual stx)
  (syntax-parse stx
    [any
     (L0:actual (L0:meta #'any)
                (syntax-e #'any))]
    ))

(define (stx->struct stx)
  (syntax-parse stx
    [((~datum proc) (proc-name formals ...) body)
     (L0:proc (L0:meta #'proc-name)
              (syntax-e #'proc-name)
              (map stx->binding (syntax-e #'(formals ...)))
              (stx->struct #'body))]
    
    [((~datum while) test-exp body)
     (L0:while (L0:meta #'while)
               (stx->struct #'test-exp)
               (stx->struct #'body))]
    
    [((~datum channels) (bindings ...) body)
     (L0:channels (L0:meta #'channels)
                  (map stx->binding (syntax-e #'(bindings ...)))
                  (stx->struct #'body))]
    
    [((~datum variables) (bindings ...) body)
     (L0:variables (L0:meta #'variables)
                   (map stx->binding (syntax-e #'(bindings ...)))
                   (stx->struct #'body))]

    [((~datum seq) bodies ...)
     (L0:seq (L0:meta #'seq)
             (map stx->struct (syntax-e #'(bodies ...))))]
    
    [((~datum store) id expr)
     (L0:store (L0:meta #'store)
               (stx->actual #'id)
               (stx->struct #'expr))]

    [((~datum put) chan expr)
     (L0:put (L0:meta #'put)
             (stx->actual #'chan)
             (stx->actual #'expr))]
    
    [((~datum get) chan)
     (L0:get (L0:meta #'get)
             (stx->actual #'chan))]
    
    ;; Application
    [(proc actuals ...)
     (L0:application (L0:meta #'proc)
                     (syntax-e #'proc)
                     (map stx->actual (syntax-e #'(actuals ...))))]

    ;; Datum? This is a bad catch-all case, but I'm unsure
    ;; at the moment how to handle numbers.
    ;; FIXME: Handle numbers better.
    [any
     (L0:num (L0:meta #'any)
             (syntax-e #'any))]
     ))
     
(define (stx-to-structs lostx)
  (L0:prog
   (map stx->struct lostx)))